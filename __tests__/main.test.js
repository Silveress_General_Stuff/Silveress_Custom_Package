const functions = require('../main.js')
const rp0 = require('request-promise-native')
const {MongoClient} = require('mongodb')
const ua0 = require("universal-analytics")
const uuid0 = require("uuid-by-string")

test('moduleTest', () => {
  expect(functions.moduleTest()).toBe('This is a successful test')
})

test('cleanQuery', () => {
  let query = { fields: 'id', marketable: 'true', ids: '100' }
  expect(functions.cleanQuery(query)).toStrictEqual({ fields: 'id', marketable: true, ids: 100 })
  query = { fields: 'id', marketable: 'false', ids: '100' }
  expect(functions.cleanQuery(query)).toStrictEqual({ fields: 'id', marketable: false, ids: 100 })
})

test('convertDate', () => {
  let date = '2019-07-01T09:47:55.884Z'
  expect(functions.convertDate(date, 'European')).toBe('01/07/2019')
  expect(functions.convertDate(date, 'American')).toBe('07/01/2019')
  expect(functions.convertDate(date, 'Other')).toBe(date)
  expect(functions.convertDate(date)).toBe(date)

  date = '2019-11-12T09:47:55.884Z'
  expect(functions.convertDate(date, 'European')).toBe('12/11/2019')
  expect(functions.convertDate(date, 'American')).toBe('11/12/2019')
  expect(functions.convertDate(date, 'Other')).toBe(date)
  expect(functions.convertDate(date)).toBe(date)
})

test('IsJsonString', () => {
  expect(functions.IsJsonString({})).toBe(true)
  expect(functions.IsJsonString([])).toBe(true)
  expect(functions.IsJsonString([{}])).toBe(true)
  expect(functions.IsJsonString('test')).toBe(false)
  expect(functions.IsJsonString()).toBe(false)
})

// weak test I know but due to setup not easy tp pin down
test('bumpRequests', () => {
  let requests = { requests: { count: 0, times: {} } }
  expect(functions.bumpRequests('test', requests)).toBeInstanceOf(Object)
})

test('capitalize', () => {
  expect(functions.capitalize('testing')).toBe('Testing')
  expect(functions.capitalize('Testing')).toBe('Testing')
  expect(functions.capitalize('tESTING')).toBe('TESTING')
  expect(functions.capitalize('TESTING')).toBe('TESTING')
  expect(functions.capitalize(true)).toBe('')
})

test('createRandomString', () => {
  expect(typeof functions.createRandomString()).toBe('string')
  expect(functions.createRandomString()).toHaveLength(50)
  expect(functions.createRandomString(100)).toHaveLength(100)
})

test('convertToGold', () => {
  let number1 = 12345
  let number2 = 12300
  let number3 = 10000
  let number4 = -12345
  let number5 = ""
  expect(typeof functions.convertToGold(number1)).toBe('string')
  expect(functions.convertToGold(number1)).toBe('1g23s45c')
  expect(functions.convertToGold(number1, true)).toBe('1g23s45c')

  expect(typeof functions.convertToGold(number2)).toBe('string')
  expect(functions.convertToGold(number2)).toBe('1g23s00c')
  expect(functions.convertToGold(number2, true)).toBe('1g23s')

  expect(typeof functions.convertToGold(number3)).toBe('string')
  expect(functions.convertToGold(number3)).toBe('1g00s00c')
  expect(functions.convertToGold(number3, true)).toBe('1g')

  expect(typeof functions.convertToGold(number4)).toBe('string')
  expect(functions.convertToGold(number4)).toBe('-1g23s45c')
  expect(functions.convertToGold(number4, true)).toBe('-1g23s45c')

  expect(typeof functions.convertToGold(number5)).toBe('string')
  expect(functions.convertToGold(number5)).toBe('0c')
  expect(functions.convertToGold(number5, true)).toBe('0c')
})

test('beautifyJSON', () => {
  let json = [{ 'id': 24, 'sell_quantity': 29140 }]
  let beautify = '[\n  {\n    "id": 24,\n    "sell_quantity": 29140\n  }\n]'
  let min = '[{"id":24,"sell_quantity":29140}]'
  expect(typeof functions.beautifyJSON(json)).toBe('string')
  expect(functions.beautifyJSON(json)).toBe(beautify)
  expect(functions.beautifyJSON(json, 'min')).toBe(min)
  expect(functions.beautifyJSON(json, 'other')).toBe(beautify)
  expect(functions.beautifyJSON(json)).toBe(beautify)
})

test('sorter', () => {
  let a = 1
  let b = 2
  let c = 'a'
  let d = 'z'
  expect(typeof functions.sorter(a, b)).toBe('number')
  expect(functions.sorter(a, b)).toBe(-1)
  expect(functions.sorter(b, a)).toBe(1)
  expect(functions.sorter(a, a)).toBe(0)

  // compare strings
  expect(functions.sorter(c, d)).toBe(-1)
  expect(functions.sorter(d, c)).toBe(1)
  expect(functions.sorter(c, c)).toBe(0)

  // sets numbers and strings to be equal
  expect(functions.sorter(a, c)).toBe(0)
  expect(functions.sorter(c, a)).toBe(0)

  // reverse
  expect(functions.sorter(a, b, true)).toBe(1)
  expect(functions.sorter(b, a, true)).toBe(-1)
  expect(functions.sorter(a, a, true)).toBe(0)
})

test('stringToNumber', () => {
  let str = '1'
  let gold = '1g23s45c'
  expect(typeof functions.stringToNumber(str)).toBe('number')
  expect(functions.stringToNumber(str)).toBe(1)

  expect(typeof functions.stringToNumber(gold)).toBe('number')
  expect(functions.stringToNumber(gold)).toBe(12345)

  expect(functions.stringToNumber()).toBe(0)
})

test('uniq', () => {
  let input = [ 'a', 'b', 'b', 'c', 1, 2, 1 ]
  let out = [ 'a', 'b', 'c', 1, 2 ]
  expect(typeof functions.uniq(input)).toBe('object')
  expect(functions.uniq(input)).toStrictEqual(out)
})

test('getNextUpdate', () => {
  let level = 1
  let mapping = { '1': 12, '2': 6, '3': 1, '4': 0.25 }

  expect(typeof functions.getNextUpdate(level, false, mapping)).toBe('string')
  expect(new Date(functions.getNextUpdate(level, false, mapping)).getTime()).toBeGreaterThan(new Date().getTime())

  // greeater than this time yesterday
  expect(new Date(functions.getNextUpdate(level, true, mapping)).getTime()).toBeGreaterThan(new Date(new Date().getTime() - (24 * 60 * 60 * 1000)).getTime())
})

test('filterGeneralNumber', () => {
  expect(typeof functions.filterGeneralNumber('100', '1,1000', 1)).toBe('boolean')
  expect(functions.filterGeneralNumber('100', '1,1000')).toBe(true)
  expect(functions.filterGeneralNumber('100', '1,', 1)).toBe(true)
  expect(functions.filterGeneralNumber('100', ',1000', 1)).toBe(true)

  expect(functions.filterGeneralNumber('1001', '1,1000', 1)).toBe(false)
  expect(functions.filterGeneralNumber('0', '1,')).toBe(false)
  expect(functions.filterGeneralNumber('1001', ',1000', 1)).toBe(false)
})

test('filterGeneralText', () => {
  expect(typeof functions.filterGeneralText('fullSubString', 'SubString')).toBe('boolean')
  expect(functions.filterGeneralText('fullSubString', 'SubString')).toBe(true)
  expect(functions.filterGeneralText('fullSubString', 'substring')).toBe(true)

  expect(functions.filterGeneralText('fullSubString', 'Subway')).toBe(false)
  expect(functions.filterGeneralText('fullSubString', 'subway')).toBe(false)

  expect(functions.filterGeneralText(undefined, 'Subway')).toBe(false)
  expect(functions.filterGeneralText('fullSubString')).toBe(false)
})



test('getURL', async() => {
  let rp = rp0
  let url = "https://api.guildwars2.com/v1/build"

  // normal fetch
  await functions.getURL(rp, url).then(data => {
    expect(typeof data.headers).not.toBe('undefined')
    expect(typeof data.body).not.toBe('undefined')
    expect(typeof data.error).toBe('undefined')
  })

  // normal with options
  await functions.getURL(rp, url, { uri: url }).then(data => {
    expect(typeof data.headers).not.toBe('undefined')
    expect(typeof data.body).not.toBe('undefined')
    expect(typeof data.error).toBe('undefined')
  })

  url = "https://notEndpoint.guildwars2.com/v1/build"
  // erroring
  await functions.getURL(rp, url, { uri: url }).then(data => {
    expect(typeof data.headers).toBe('undefined')
    expect(typeof data.body).toBe('undefined')
    expect(typeof data.error).not.toBe('undefined')
  })
})

test('sendPageView', async() => {
  let ua = ua0
  let uuid = uuid0
  let req = {
    userAgent:()=>"testing Device",
    headers:{
      "accept-language":"*",
      "x-forwarded-for": "localhost"
    },
    href:()=>"localhost"
  }
  // actually works fine without this for some reason
  let ga = ""

  // normal fetch
  let result = functions.sendPageView(ua, uuid, req, ga)
  expect(typeof result).toBe('boolean')
  expect(result).toBe(true)
})

test('dataToDbArray', async() => {
  let data = [{id:1,name:"one"},{id:2,name:"two"}]

  let result, output
  result = functions.dataToDbArray(data)
  output = {"0":[{"updateOne":{"update":{"$set":{"id":1,"name":"one"}},"upsert":true,"filter":{"id":1}}},{"updateOne":{"update":{"$set":{"id":2,"name":"two"}},"upsert":true,"filter":{"id":2}}}],"arrays":0}
  expect(typeof result).toBe('object')
  expect(result).toStrictEqual(output)

  result = functions.dataToDbArray(data,1,"name")
  output = {"0":[{"updateOne":{"update":{"$set":{"id":1,"name":"one"}},"upsert":true,"filter":{"name":"one"}}}],"1":[{"updateOne":{"update":{"$set":{"id":2,"name":"two"}},"upsert":true,"filter":{"name":"two"}}}],"arrays":1}
  expect(typeof result).toBe('object')
  expect(result).toStrictEqual(output)

  result = functions.dataToDbArray(data,1,"name", { firstAdded: "2019-07-03T14:28:41.519Z" })
  output = {"0":[{"updateOne":{"update":{"$set":{"id":1,"name":"one"},"$setOnInsert":{"firstAdded":"2019-07-03T14:28:41.519Z"}},"upsert":true,"filter":{"name":"one"}}}],"1":[{"updateOne":{"update":{"$set":{"id":2,"name":"two"},"$setOnInsert":{"firstAdded":"2019-07-03T14:28:41.519Z"}},"upsert":true,"filter":{"name":"two"}}}],"arrays":1}
  expect(typeof result).toBe('object')
  expect(result).toStrictEqual(output)
})

describe('Mongodb', () => {
  let connection;
  let db;

  beforeAll(async () => {
    connection = await MongoClient.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
    });
    db = await connection.db(global.__MONGO_DB_NAME__);
  });

  afterAll(async () => {
    await connection.close();
    await db.close();
  });

  let sessionKey
  it('createUniqueSession', async () => {
    let collection = "accounts_sessions"
    let user = "testUser.1234"
    let req = {
      userAgent:()=>"testing Device",
      headers:{
        "accept-language":"*",
        "x-forwarded-for": "localhost"
      },
      href:()=>"localhost"
    }

    sessionKey = await functions.createUniqueSession(db, collection, user, req)
    expect(typeof sessionKey).toBe("string")

    sessionKey = await functions.createUniqueSession(db,undefined , user, req)
    expect(typeof sessionKey).toBe("string")

    let sessionKey_test = "testSessionKey"
    sessionKey = await functions.createUniqueSession(db,undefined , user, req, sessionKey_test)
    expect(typeof sessionKey).toBe("string")

    sessionKey = await functions.createUniqueSession(db,undefined , user, req, sessionKey_test)
    expect(sessionKey).not.toBe(sessionKey_test)
  })


  it('verifySession', async () => {
    let collection = "accounts_sessions"
    let req = {
      userAgent:()=>"testing Device",
      headers:{
        "accept-language":"*",
        "x-forwarded-for": "localhost"
      },
      href:()=>"localhost"
    }

    let result
    result = await functions.verifySession(db, collection, sessionKey, req)
    expect(typeof result).toBe("object")
    expect(typeof result.error).toBe("undefined")

    result = await functions.verifySession(db, undefined, sessionKey, req)

    expect(typeof result).toBe("object")
    expect(typeof result.error).toBe("undefined")

    result = await functions.verifySession(db, undefined, "sessionKey", req)
    expect(typeof result).toBe("object")
    expect(typeof result.error).not.toBe("undefined")

  })

  it('logToDB', async () => {
    let logCollection = "logging2"
    let type = "type"
    let location = "location"
    let message = "message"
    let misc = {
      message2:"this is message2"
    }
    let consoleFlag = true
    let maxSize = 5

    let result
    result = await functions.logToDB(db, logCollection, type, location, message, misc, consoleFlag, maxSize)
    expect(typeof result).toBe("undefined")

    result = await functions.logToDB(db, undefined, type, location, message, undefined, false, undefined)
    expect(typeof result).toBe("undefined")

    result = await functions.logToDB(db, logCollection, type, location, message, undefined, consoleFlag, maxSize)
    expect(typeof result).toBe("undefined")

    result = await functions.logToDB(db, logCollection, type, location, message, "undefined", consoleFlag, maxSize)
    expect(typeof result).toBe("undefined")



  })

});